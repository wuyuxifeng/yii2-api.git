<?php
namespace wuyuxifeng\api\controllers;
use yii;  
use yii\filters\auth\QueryParamAuth;  
use yii\filters\auth\CompositeAuth;  
use yii\filters\RateLimiter;
use wuyuxifeng\api\ApiPackage;
use wuyuxifeng\api\IApi;
use wuyuxifeng\api\ErrorHandler;
use wuyuxifeng\api\Response;
use wuyuxifeng\api\exception\InvalidApiException;

/**
 * 负责调用的控制器
 */
class IndexController extends \yii\base\Controller{ 
	static public $moduleIns=null;
	private $clazz;

	/**
	 * 注册异常捕获类，用于接管抛出的异常
	 *
	 * @return void
	 */
	function init(){		  
		$this->layout=false;	
		IndexController::$moduleIns=$this->module;		
		$handler=Yii::createObject($this->module->errorHandlerClass);	 
		$handler->register();	
		$method=Yii::$app->request->get('method',null);		 
		$v=Yii::$app->request->get('v',$this->module->defaultVersion);	 
		if($method===null){
			throw new InvalidApiException('请传入方法');
		}
		$apis=$this->module->apiConfig;
		if(!isset($apis[$v])){
			throw new InvalidApiException('版本号不存在');
		} 

		$methodArr=explode('.',$method);
		if(!isset($apis[$v][$method])){ 
			//接口模块级别查找
			$cate=$apis[$v][current($methodArr)];
			if(isset($cate['autoScanNamespace'])){
				$findClazz=array_slice($methodArr,1,count($methodArr));
				$findClazz[count($findClazz)-1]=\yii\helpers\Inflector::camelize(end($findClazz));
				$findClazz=$cate['autoScanNamespace'].'\\'.join('\\',$findClazz);
				if(class_exists($findClazz)){
                    $this->clazz=Yii::createObject($findClazz);
                    if(!$this->clazz instanceof IApi) throw new InvalidApiException('这不是一个IApi的实例');
                    return true;                    
                }  
			}			
			
			//模块配置查找
            if(empty($this->module->autoScanNamespace)) throw new InvalidApiException('找不到API:'.$method);
            $ms=$methodArr;
            $ms[count($ms)-1]=ucfirst(end($ms));
            $ms=join('\\',$ms);
            foreach($this->module->autoScanNamespace as $item){
                $path=$item.'\\'.$ms;
                if(class_exists($path)){
                    $this->clazz=Yii::createObject($path);
                    if(!$this->clazz instanceof IApi) throw new InvalidApiException('这不是一个IApi的实例');
                    return true;                    
                } 
            } 
			throw new InvalidApiException('找不到API:'.$method);
		}


		$classz=$apis[$v][$method];
		if(is_string($classz)){
			$classz=['class'=>$classz];
		}
		if(!class_exists($classz['class'])){
			throw new InvalidApiException('找不到class:'.$classz['class']);
		}

		$this->clazz=Yii::createObject($classz);
		if(! $this->clazz instanceof IApi){
			throw new InvalidApiException('这不是一个IApi的实例');
		}
		 
	}
 
	/**
	 * invoke method
	 *
	 * @return void
	 */
	function actionIndex(){ 	 
		$return=$this->clazz->prepareCheck(); 	 
		if($this->clazz->beforeHandle($this->clazz->attributes)){
			$return=$this->clazz->handle($this->clazz->attributes); 			
		}else{
			throw new InvalidApiException('强制终止运行');
		}
		$return=$this->clazz->afterHandle($return);	 
		echo $this->module->getResponse()->renderSuccess($return); 	 
		exit;
	} 

	/**
	 * 验证的行为,包含  回话的验证，速率的验证
	 *
	 * @return void
	 */
	public function behaviors()	{
		$behaviors = parent::behaviors();
		if($this->clazz->auth==true){ 		
	 
			$as=explode(',',$this->module->authType);	
			$authMethods=[];
			foreach($as as $i) {		
				$v=$this->module->builtInAuthTypes[$i];
				if(empty($v)) throw new InvalidApiException('无效的authType参数:'.$i);
				$authMethods[]=$v;
			}	
			$behaviors['auth'] = [
				'class'=>CompositeAuth::class,
				'authMethods'=>$authMethods
			];			
		}	
		return $behaviors;
	}
 
}
