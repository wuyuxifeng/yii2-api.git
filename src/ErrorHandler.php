<?php
namespace wuyuxifeng\api;
use yii;
use yii\web\UnauthorizedHttpException;
use wuyuxifeng\api\exception\Exception;
use wuyuxifeng\api\Response;
use wuyuxifeng\api\exception\InvalidApiException;
use wuyuxifeng\api\controllers\IndexController;
 
class ErrorHandler extends \yii\web\ErrorHandler
{ 
    /**
     * 输出接管
     *
     * @param Exception $exception
     * @return void
     */
    protected function renderException($e){            
        $code=500;
        $res=IndexController::$moduleIns->getResponse();
        if($e instanceof UnauthorizedHttpException){
            $code=401;
        } 
        echo $res->renderException($e,$code);  
        exit;
    }
}
